\chapter{Categorias de Espaços}

Neste capítulo apresentaremos espaços fundamentais e convenientes para o desenvolvimento da teoria de homotopia, bem como construções de novos espaços, como \textit{simplexos} e \textit{CW-complexos}, à partir dos fundamentais, com o objetivo de caracterizar subcategorias de $\Top$ nas quais os teoremas mais importantes da teoria se apliquem no geral, eliminando assim casos patológicos.

\section{Esferas e Discos}

\begin{defi}
A \textbf{esfera} $n$-dimensional é o espaço

\[
	S^n = \left\{x \in \R^{n + 1} \| |x| = 1 \right\}
\]

O \textbf{disco} $(n+1)$-dimensional é o espaço

\[
	D^{n+1} = \left\{x \in \R^{n+1} \| |x| \le 1 \right\}
\]

O \textbf{hemisfério norte} de $S^n$ é o espaço 
\[
	D_N^n = \left\{x \in \R^{n+1} \| |x| = 1 \text{ e } x_{n+1} \ge 0 \right\}
\] 
e o \textbf{hemisfério sul} é
\[
	D_S^n = \left\{x \in \R^{n+1} \| |x| = 1 \text{ e } x_{n+1} \le 0 \right\}
\]
\end{defi}

\begin{defi}
Sejam $x,y \in \R^n$. O \textbf{segmento} de reta entre $x$ e $y$ é o conjunto
\[
	\overline{xy} = \left\{ (1-t)x + ty \| 0 \le t \le 1 \right\}
\]
\end{defi}

\begin{defi}
Um subconjunto $C \subset \R^n$ é dito \textbf{convexo} se, dados $x,y \in C$, $\overline{xy} \subset C$. Caso $C$ contenha toda a reta determinada por $x$ e $y$, então é dito \textbf{afim}.
\end{defi}

\begin{obs}
	Claramente qualquer conjunto afim é um conjunto convexo. A recíproca não é verdadeira, pois basta tomar o exemplo do disco $D^2$ que é convexo, mas é limitado e portanto não pode conter um conjunto ilimitado, em particular, a reta determinada por $x$ e $y$.
\end{obs}

\begin{prop}
	A interseção arbitrária de conjuntos convexos é um conjunto convexo.
\end{prop}

\begin{proof}
	Seja $\objfamily{C}{I}{i}$ uma família de conjuntos convexos e $C = \bigcap_i C_i$. Dados $x,y \in C$, então $x,y \in C_i$ para todo $i \in I$. Como cada $C_i$ é convexo, $\overline{xy} \subset C_i$ para todo $i \in I$. Portanto, $\overline{xy} \subset C$.
\end{proof}

\begin{defi}
	Seja $A \subset \R^n$ e $\objfamily{A}{I}{i}$ a família de todos os $A_i \subset \R^n$ convexos tais que $A \subset A_i$ para todo $i \in I$ então $\bigcap_i A_i$ é a \textbf{envoltória convexa} de $A$. Em outras palavras, a envoltória convexa de $A$ é a interseção de todos os conjuntos convexos que contém $A$.
\end{defi}

\begin{defi}
	Um p-\textbf{simplexo} $s \subset \R^n$ é a envoltória convexa de uma coleção de $p + 1$ pontos $\{x_0, \ldots, x_p\} \subset \R^n$ de forma que o conjunto $\{x_1 - x_0,\ldots, x_p - x_0\}$ é linearmente independente. Tal definição é independente da escolha de $x_0$.
\end{defi}

\begin{prop}
	Seja $\{x_0, \ldots, x_p\} \subset \R^n$, então são equivalentes
	
	\begin{enumerate}[label=(\alph*)]
		\item $\{x_1 - x_0,\ldots, x_p - x_0\}$ é linearmente independente.
	    \item Se $\sum_{i = 0}^p s_i x_i = \sum_{i = 0}^p t_i x_i$ e $\sum_{i = 0}^p s_i = \sum_{i = 0}^p t_i$, então $s_i = t_i$ para todo $i = 0,\ldots,p$
	\end{enumerate}
\end{prop}

\begin{proof} $(a) \implies (b)$: Se $\sum_{i = 0}^p s_i x_i = \sum_{i = 0}^p t_i x_i$ e $\sum_{i = 0}^p s_i = \sum_{i = 0}^p t_i$, então

\begin{align*}
0 &= \sum_{i = 0}^p (s_i - t_i) x_i = \sum_{i = 0}^p (s_i - t_i) x_i - \left[\sum_{i = 0}^p (s_i - t_i) \right] x_0 \\
  &= \sum_{i = 0}^p (s_i - t_i) x_i - \sum_{i = 0}^p (s_i - t_i) x_0 \\
  &= (s_0 - t_0) x_0 + \sum_{i = 1}^p (s_i - t_i) x_i - (s_0 - t_0) x_0 - \sum_{i = 1}^p (s_i - t_i) x_0 \\
  &= \sum_{i = 1}^p (s_i - t_i) (x_i - x_0)
\end{align*}
%
Por $(a)$ concluímos que $s_i - t_i = 0$ para todo $i = 1,\ldots,p$, ou seja, $s_i = t_i$ para todo $i = 1,\ldots,p$. Como $\sum_{i = 0}^p s_i = \sum_{i = 0}^p t_i$, temos que $s_0 = t_0$. Portanto $s_i = t_i$ para todo $i = 0,\ldots,p$.

$(b) \implies (a)$: Considere a combinação linear

\[
\sum_{i = 1}^p t_i (x_i - x_0) = 0
\]
%
daí temos que $0 x_0 + \sum_{i = 1}^p t_i x_i = \left[\sum_{i = 1}^p t_i \right] x_0 + 0 x_1 + \ldots + 0 x_p$, dessa forma estamos nas condições da hipótese $(b)$, isto é, $\sum_{i = 0}^p s_i x_i = \sum_{i = 0}^p t_i x_i$ e $\sum_{i = 0}^p s_i = \sum_{i = 0}^p t_i$. Concluímos então que $t_i = s_i = 0$ para todo $i \in 1,\ldots,p$ e portanto $\{x_1 - x_0,\ldots, x_p - x_0\}$ é linearmente independente.	
\end{proof}

\section{CW-Complexos}

CW-Complexos são espaços construídos de forma indutiva com relação a dimensão dos espaços que os compõem, isto é, começamos a construção com um espaço de uma determinada dimensão e acrescentamos à ele outros espaços de dimensões iguais ou maiores através de um processo de \textit{colagem}. Neste seção vamos construir os conceitos necessários para formalizar essa idéia de colagem e, consequentemente, os CW-complexos.

Várias construções realizadas neste capítulo dependem do conceito de topologia quociente, isto é, dado um espaço $X$ e uma relação de equivalência $\sim$ em $X$, queremos definir um topologia em $X/\eqv$ que possua propriedades semelhantes às da topologia de $X$. Começamos então verificando quais condições uma relação de equivalência deve satisfazer para que isso ocorra.

\begin{defi}
	Sejam $X \in \Top$ e $X' = \{X_j \subseteq X \| j \in J\}$ uma partição de $X$. A \textbf{aplicação quociente}
	\begin{align*}
		\nu : X &\to X' \\
		x &\mapsto X_j
	\end{align*}
	onde $X_j$ é o único subconjunto na partição tal que $x \in X_j$. A \textbf{topologia quociente} em $X'$ é a família
	\[
		\left\{U' \subseteq X' \| \nu^{-1}(U') \text{ é aberto em } X \right\}
	\]
	É fácil ver que $\nu$ é contínua quando $X'$ possui a topologia quociente.
\end{defi}

Temos dois casos importantes dessa definição. No primeiro, se $A \subseteq X$, tomamos
\[
	X' = \left\{ A \right\} \cup \left( \bigcup_{i \in I} \, \{x_i\} \right), \quad x_i \in X \setminus A, \text{ para todo } i \in I
\]
e denotamos $X'$ por $X/A$. No segundo caso, quando temos uma relação de equivalência $\eqv$ em $X$. Neste caso $X'$ é o conjunto das classes de equivalência por $\eqv$, denotado por $X/\eqv$. Sabemos que tal conjunto particiona $X$ e a aplicação quociente é dada por $\nu: x \mapsto [x]$ onde $[x]$ denota a classe de equivalência do elemento $x \in X$.

\begin{defi}
	Uma aplicação $f : X \to Y$ contínua e sobrejetora é uma \textbf{identificação} se para qualquer $U \subseteq Y$ temos que
	\[
		U \text{ é aberto em } Y \iff f^{-1}(U) \text{ é aberto em } X
	\]
\end{defi}

Denotamos a \textbf{diagonal} de $X \times X$, onde $X \in \Top$ por $\Delta_X = \{(x,x) \| x \in X\}$. Sabemos, da topologia geral, que um espaço $X$ é Hausdorff se, e somente se, $\Delta_X$ é fechado em $X \times X$.

\begin{defi}
	O \textbf{gráfico} de uma relação binária $\eqv$ em um espaço $X$ é o conjunto
	\[
		G = \left\{(x_1,x_2) \in X \times X \| x_1 \eqv x_2 \right\}.
	\]
	Dizemos que $\eqv$ é \textbf{fechada} se seu gráfico é um conjunto fechado de $X \times X$.
\end{defi}

Podemos reformular o comentário acima de seguinte maneira: $X$ é espaço de Hausdorff se, e somente se, a relação identidade é fechada em $X$.

Seja $\eqv$ uma relação de equivalência em um espaço $X$, então seu gráfico $G$ é igual à $(\nu \times \nu)^{-1}(\Delta_{X/\eqv})$, em que $\nu : X \to X/\eqv$ é a aplicação quociente, pois
\begin{align*}
	(\nu \times\nu)^{-1}(\Delta_{X/\eqv}) 
		&= \{(x_1, x_2) \in X \times X \| (\nu \times \nu)(x_1,x_2) = ([x_1],[x_2]) \in \Delta_{X/\eqv} \} \\
		&= \{(x_1, x_2) \in X \times X \| [x_1] = [x_2] \} \\
		&= \{(x_1, x_2) \in X \times X \| x_1 \sim x_2 \} \\
		&= G.
\end{align*}
Se $X/\eqv$ é Hausdorff, sabemos que $\Delta_{X/\eqv}$ é fechado em $X/\eqv \times X/\eqv$, logo $G$ é fechado em $X \times X$, pois $\nu$ é contínua e consequentemente $\nu \times \nu$ também o é. Portanto, $\eqv$ é fechada. A recíproca é dada pelos seguintes resultados.

\begin{lema}
	Sejam $\nu : W \to Z$ uma aplicação fechada, $S \subseteq Z$ e $U$ aberto de $W$ tal que $\nu^{-1}(S) \subseteq W$. Então existe um aberto $V \subseteq Z$ tal que
	\[
		S \subseteq V \quad \text{e} \quad v^{-1}(V) \subseteq U
	\]
\end{lema}

\begin{teo}
	Se $\eqv$ é uma relação de equivalência em um espaço de Hausdorff compacto $X$, então o espaço quociente $X/\eqv$ também é Hausdorff compacto.
\end{teo}

\begin{coro}
	Se $X$ é um espaço de Hausdorff compacto e $A \subseteq X$ fechado, então $X/A$ é Hausdorff compacto.
\end{coro}

Definiremos agora formalmente o procedimento de colagem de espaços, com o objetivo de construir novos espaços através de outros mais simples e com propriedades conhecidas permitindo a obtenção de informações sobre o espaço resultante.

\begin{defi}
Seja $\sim'$ é uma relação binária em $X$. Para $x,y \in X$ definimos que $x \eqv y$ exatamente quando existirem $x_0, \ldots, x_n$ com $x_0 = x$ e $x_n = y$ tais que
%
\begin{enumerate}[label=(\roman*)]
	\item $x_{i+1} = x_i$, ou
	\item $x_{i+1} \sim' x_i$, ou
	\item $x_i \sim' x_{i+1}$
\end{enumerate}
%
para todo $i = 0, \ldots, n$. A relação $\sim$ é dita a relação de equivalência \textbf{gerada} por $\sim'$.
\end{defi}

\begin{defi}
Sejam $X,Y \in Top$, $A \subseteq X$ e $f : A \to Y$ uma aplicação contínua. O espaço obtido de $Y$ através da \textbf{colagem} de $X$ por $f$ é definido por $X \coprod Y/\eqv$ onde $\eqv$ é a relação de equivalência gerada por $\{a \eqv f(a) \| a \in A \}$. Tal espaço é denotado por
\[
	X \coprod_f Y
\]
e $f$ é chamada de \textbf{aplicação colagem}.
\end{defi}

\begin{defi}
	Considere as aplicações inclusão $i$ e quociente $\nu$ no diagrama
	
	\adjustbox{scale=\diagramscale, center} {
		\begin{tikzcd}
			X \arrow[hookrightarrow]{r}{i}
			& X \coprod Y \ar[r, "\nu"]
			  &  X \coprod_f Y
		\end{tikzcd}
	}
	Definimos a \textbf{aplicação característica} por
	\begin{align*}
		\Phi &= \nu \circ i
	\end{align*}
\end{defi}

\begin{teo}
	Sejam $X$ e $Y$ espaços de Hausdorff, $A \subset X$ compacto e $f : A \to Y$ contínua. Então $X \coprod_f Y$ é Hausdorff.
\end{teo}

\begin{defi}
	Uma \textbf{$n$-célula} $e^n$ é uma cópia homeomorfa de um disco aberto $D^n \setminus S^{n-1}$. Uma \textbf{$n$-célula fechada} é uma cópia homeomorfa do disco $D^n$.
\end{defi}

Apresentaremos agora, uma série de definições e resultados técnicos necessários para a definição precisa de CW-complexos.

\begin{defi}
	Um espaço de Hausdorff $X$ é dito \textbf{localmente compacto} se para cada $x \in X$ e para toda vizinhança $U$ de $x$, existe uma aberto $W$ cujo fecho $\topclosure{W}$ é compacto e $x \in W \subset \topclosure{W} \subset U$.
\end{defi}

\begin{lema}
	Se $\nu : X \to X'$ é uma identificação e $Z$ é Hausdorff localmente compacto, então $\nu \times \id_Z : X \times Z \to X' \times Z$ é uma identificação.
\end{lema}

\begin{defi}
	Seja $X$ um conjunto $\objfamily{A}{J}{j}$ uma família de subconjuntos de $X$, tal que $X = \bigcup_{j \in J} A_j$, tal que
	
	\begin{enumerate}[label=(\roman*)]
		\item $A_j \in \Top$ para todo $j \in J$;
		\item As topologias de $A_j$ e $A_k$ são compatíveis em $A_j \cap A_k$, para todo $j,k \in J$;
		\item $A_j \cap A_k$ é fechado em $A_j$ e $A_k$, para todo $j,k \in J$.
	\end{enumerate}
	
	Então a \textbf{topologia fraca} em $X$ \textbf{determinada} por $\objfamily{A}{J}{j}$ é a topologia cujos conjuntos fechados são os subconjuntos $F \subset X$ tais que $F \cap A_j$ é fechado em $A_j$ para todo $j \in J$.
\end{defi}

\begin{defi}
	Seja $X \in \Top$ e $E$ uma cobertura de $X$ por células, isto é, $X = \bigcup \{e \| e \in E\}$. Para cada $k \ge 0$, o \textbf{$k$-esqueleto} $X^{(k)}$ é definido por
	\[
		X^{(k)} = \bigcup \left\{ e \in E \| \dim(e) \le k \right\}.
	\]
\end{defi}

\begin{defi}
	Uma aplicação contínua $g : (X,A) \to (Y,B)$ é um \textbf{homeomorfismo relativo} se $\restr{g}{X \setminus A} : X \setminus A \to Y \setminus B$ é um homeomorfismo.
\end{defi}

\begin{defi}
	Um \textbf{CW-complexo} é uma terna $(X, E, \Phi)$ onde $X$ é um espaço de Hausdorff, $E$ é uma cobertura de $X$ por células e $\Phi = \{\Phi_e \| e \in E\}$ uma família de aplicações, tal que

	\begin{enumerate}[label=(\roman*)]
		\item 
		$X = \bigcup \left\{ e \| e \in E \right\}$;
		
		\item
		para cada $k$-célula $e \in E$, a aplicação $\Phi_e: (D^k, S^{k-1}) \to (e \cup X^{(k-1)}, X^{(k-1)})$ é um homeomorfismo relativo;
		
		\item
		se $e \in E$, então seu fecho $\topclosure{e}$ está contido em uma união finita de células de $E$;
		
		\item
		$X$ possui a topologia fraca determinada por $\{ \topclosure{e} \| e \in E \}$.
	\end{enumerate}
	$X$ é chamado de \textbf{espaço CW}, $(E,\Phi)$ é chamada de \textbf{decomposição CW} e $\Phi_e \in \Phi$ é chamada de \textbf{aplicação característica} de $e$. Caso $E$ seja finito, então o CW-complexo é \textbf{finito}.
\end{defi}

\section{A categoria de pares}

Seja $\TopP$ a categoria cujos objetos são pares $(X,A)$ onde $X$ é um espaço topológico e $A$ é um subespaço de $X$. Os morfismos de $\TopP$, denotados por
\[
f : (X,A) \to (Y,B)
\]
são pares ordenados $(f,f')$, tais que $f : X \to Y$ é uma aplicação contínua e o seguinte diagrama comuta

\adjustbox{scale=\diagramscale, center} {
	\begin{tikzcd}
		A \arrow[hookrightarrow]{r}{i_A} \ar[d, swap, "f'"]
		& X \ar[d, "f"]
		\\
		B \arrow[hookrightarrow]{r}[swap]{i_B}
		& Y
	\end{tikzcd}
}
onde $i_A$ e $i_B$ são as inclusões de $A$ e $B$, respectivamente, e a composição é feita em cada coordenada. Essa definição de morfismos equivale a exigir que $f : X \to Y$ seja contínua e que $f(A) \subseteq B$. $\TopP$ é chamada de categoria de \textbf{pares} (de espaços topológicos). 

\section{Espaços com ponto base}

Consideremos a subcategoria $\TopB$ de $\TopP$, cujos objetos são pares $(X,A)$ com $A = \{x_0\}$ e $x_0 \in X$. Os pares são denotados $(X,x_0)$ e são chamados \textbf{espaços pontuados} (ou \textbf{espaços com ponto base}). $x_0$ é chamado de \textbf{ponto base} de $(X,x_0)$ e os morfismos são chamados de \textbf{aplicações pontuadas}. $\TopB$ é chamada categoria dos \textbf{espaços pontuados}. Quando não desejamos fazer distinção entre pontos base específicos, utilizamos um ponto base formal (abstrato) $\star$.

Como $\TopB$ é subcategoria de $\TopP$, temos que para $(X,x_0), (Y,y_0) \in \TopB$ a condição sobre os morfismos em $\TopP$ se torna
$$
f(\{x_0\}) \subseteq \{y_0\} \iff f(x_0) = y_0.
$$
Portanto os morfismos de $\TopB$ (aplicações pontuadas) são aqueles que preservam pontos base.

\section{Relação entre espaços com ponto base e espaços sem ponto base}

Muitas das construções homotópicas a serem realizadas à diante se aplicam de maneira análoga à espaços com ponto base e espaços sem ponto base, isto é, espaços topológicos quaisquer. Devido a isso buscamos relações entre esses espaços, para criar uma teoria geral que se aplique a ambos os casos. A primeira dessas relações vem da observação de que a categoria $\Top$ pode ser vista como subcategoria de $\TopP$ identificando cada espaço $X \in \Top$ com o par $(X, \emptyset) \in \TopP$. Sendo assim a categoria $\TopP$ engloba ambos os casos que desejamos tratar.

Isso pode ser feito de maneira precisa considerando dois funtores, cujas funções entre objetos são dadas por
\begin{align*}
		F : \Top &\to \TopB \\
		X &\mapsto X \coprod \star
\end{align*}
onde $\star$ representa uma escolha de ponto base qualquer, e
\begin{align*}
G : \TopB &\to \Top \\
(Y,\star) &\mapsto Y.
\end{align*}
$(F,G)$ é um par de funtores adjuntos.

\section{Espaços Projetivos}

\begin{defi}
	Um \textbf{anel de divisão} é uma estrutura $(R,+,\cdot,0,1)$ tal que
	\begin{enumerate}[label=(\roman*)]
		\item $(R,+,0)$ é um grupo comutativo
		\item $(R \setminus \{0\}, \cdot, 1)$ é um grupo (não necessariamente comutativo)
		\item Vale a propriedade distributiva de $\cdot$ sobre $+$
	\end{enumerate}
\end{defi}

\begin{exem}
	Todo corpo é um anel de divisão. Em particular, $\R$ e $\C$ são anéis de divisão. Além disso, todo anel de divisão finito é um corpo.
\end{exem}

\begin{exem}
	O conjunto $\Hn$ dos quaterniões de Hamilton é um anel de divisão.
\end{exem}

\begin{defi}
	Seja $F$ um anél de divisão e $n \ge 0$. Defina uma relação de equivalência em $F^{n+1} \setminus \{0\}$ por
	\[
		x \eqv y \iff \text{existe } \lambda \in F \setminus \{0\}, \text{ tal que } x = \lambda y.
	\]
	Definimos o \textbf{$n$-espaço $F$-projetivo}, $FP^n$ por
	\[
		FP^n = (F^{n+1} \setminus \{0\})/\eqv,
	\]
	e denotamos a classe de equivalência de $x = (x_0,\cdots,x_n)$ por
	\[
		[x] = [x_0,\cdots,x_n] \in FP^n.
	\]
\end{defi}

Para todo $n \ge 0$, podemos definir a inclusão
\begin{align*}
	i : FP^n &\to FP^{n+1} \\
	(x_0,\cdots,x_n) &\mapsto [x_0,\cdots,x_n,0].
\end{align*}
Assim podemos definir o \textbf{espaço $F$-projetivo de dimensão infinita}, $FP^\infty$, por
\[
	FP^\infty = \bigcup_{n \ge 0} FP^n,
\]
mas como podemos fazer apenas uniões de famílias de subconjuntos de um conjunto dado, a definição precisa da união acima depende do conceito categórico de \textit{limite direto} que não trataremos neste trabalho.

O espaço projetivo $\R P^n$ também pode ser construído da seguinte maneira: Seja
\[
	x \eqv y \iff x = \pm y
\]
uma relação de equivalência na esfera $S^n$. Então $S^n/\eqv \catiso \R P^n$, isto é, $S^n/\eqv$ é homeomorfo à $\R P^n$.

\begin{teo}
	Para todo $n \ge 0$, os espaços projetivos $\R P^n$, $\C P^n$, são Hausdorff compactos.
\end{teo}

%\section{Exercícios}