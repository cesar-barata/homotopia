\chapter{Homotopia}

É frequente em várias áreas da matemática substituir uma função por outra que também possua algumas propriedades de interesse da função original. O conceito de homotopia surge da idéia intuitiva de distorcer uma função em outra de maneira que tal distorção possa fornecer uma função mais simples que seja semelhante à original.

\section{Homotopia de Aplicações}

\begin{defi}
	Sejam $X, Y \in \Top$ e $f_0, f_1 \in \mor_\Top(X,Y)$ aplicações contínuas, então $f_0$ é \textbf{homotópica} à $f_1$, denotado por $f_0 \homotiso f_1$, se existe uma aplicação contínua $F : X \times \I \to Y$ tal que
	\[
		F(x, 0) = f_0(x) \quad \text{e} \quad F(x,1) = f_1(x) \quad \text{para todo } x \in X.
	\]
	$F$ é chamada \textbf{homotopia} e a relação de homotopia pode ser escrita $F : f_0 \homotiso f_1$ para explicitar a homotopia.
\end{defi}

\begin{lema}[da colagem]
	Seja $X \in \Top$ e $\{X_1, \cdots, X_n\}$ uma família finita de subconjuntos fechados de $X$ que o cobre, isto é
	\[
		X = \bigcup_{i = 1}^n X_i
	\]
	Para qualquer $Y \in \Top$ tal que existem $f_i : X_i \to Y$ com
	\[
		\restr{f_i}{X_i \cap X_j} = \restr{f_j}{X_j \cap X_i} \quad \text{para todo } i, j = 1,\cdots,n
	\]
	então existe uma única aplicação contínua $f : X \to Y$ tal que
	\[
		\restr{f}{X_i} = f_i \quad \text{para todo } i = 1,\cdots,n
	\]
\end{lema}

\begin{teo}
	Homotopia é uma relação de equivalência em cada conjunto $\mor_\Top(X,Y)$.
\end{teo}

\begin{proof}
	Seja $f : X \to Y$. Definimos então
	\begin{align*}
		F : X \times \I &\to Y \\
		(x,t) &\mapsto f(x)
	\end{align*}
	para todo $x \in X$ e todo $t \in \I$. $F$ claramente é contínua, logo $F : f \homotiso f$.
	
	Assumindo agora que $F : f \homotiso g$, podemos definir
	\begin{align*}
		G : X \times \I &\to Y \\
		(x,t) &\mapsto F(x, 1-t).
	\end{align*}
	Como $F$ é contínua, $G$ também o é. Logo $G : g \homotiso f$.
	
	Agora, assumimos que $F : f \homotiso g$ e $G : g \homotiso h$. Definindo
	\begin{align*}
		H : X \times \I &\to Y \\
		(x,t) &\mapsto \begin{cases}
		F(x,2t) 		& t \in \left[0,\frac{1}{2}\right] \\
		G(x,2t - 1) 	& t \in \left[\frac{1}{2},1\right].
		\end{cases}
	\end{align*}
	Como $F$ e $G$ coincidem em $\{(x,\frac{1}{2}) \| x \in X \}$, $H$ é contínua pelo lema da colagem. Portanto $H : f \homotiso h$.
	Com isso concluímos que homotopia é uma relação de equivalência.
\end{proof}

\begin{defi}
	Seja $f \in \mor_\Top(X,Y)$, sua \textbf{classe de homotopia} é a classe de equivalência
	\[
		[f] = \left\{ g \in \mor_\Top(X,Y) \| g \homotiso f \right\}
	\]
	
	A família de todas as classes de homotopia entre $X$ e $Y$ é denotada por $[X, Y]$.
\end{defi}

\begin{teo}
	Sejam $f_i : X \to Y$ e $g_i : Y \to Z$ para $i=0,1$, aplicações contínuas. Se $f_0 \homotiso f_1$ e $g_0 \homotiso g_1$, então
	\[
		g_0 \circ f_0 \homotiso g_1 \circ f_1 \quad \text{ou equivalentemente} \quad [g_0 \circ f_0] = [g_1 \circ f_1]
	\]
\end{teo}

\begin{proof}
	Sejam $F : f_0 \homotiso f_1$ e $G : g_0 \homotiso g_1$ homotopias. Primeiramente, temos que
	\[
		g_0 \circ f_0 \homotiso g_1 \circ f_0,
	\]
	pois fazendo
	\begin{align*}
		H : X \times \I &\to Y \\
		(x,t) &\mapsto G(f_0(x), t),
	\end{align*}
	temos que
	\begin{gather*}
		H(x,0) = G(f_0(x),0) = g_0(f_0(x)) \\
		H(x,1) = G(f_0(x),1) = g_1(f_0(x))
	\end{gather*}.
	
	Agora tomamos $K = g_1 \circ F$. Portanto $K : X \times \I \to Z$ e
	\begin{gather*}
		K(x,0) = g_1(F(x,0)) = g_1(f_0(x)) \\
		K(x,1) = g_1(F(x,1)) = g_1(f_1(x))
	\end{gather*}.
	Isto é, $K : g_1 \circ f_0 \homotiso g_1 \circ f_1$.
	Pela transitividade da homotopia, temos o resultado desejado.
\end{proof}

\begin{coro}
	Homotopia é uma congruência em $\Top$.
\end{coro}

\begin{defi}
	Pelos resultados acima podemos definir $\hTop$, a \textbf{categoria de homotopia} de $\Top$, por
	\begin{align*}
		\ob{\hTop} &= \ob{\Top} \\
		\mor_\hTop(X,Y) = \left[X,Y\right] &\quad \text{para todo } X,Y \in \ob{\hTop}
	\end{align*}
	onde a composição em $[X,Y]$ é dada por $[f] \circ [g] = [f \circ g]$.
\end{defi}

Analogamente, a homotopia relativa estabelece uma congruência em $\TopB$, o que nos permitir definir $\hTopB$, a categoria de homotopia de $\TopB$.

\begin{defi}
	Uma aplicação contínua $f \in \mor_\Top(X,Y)$ é uma \textbf{equivalência homotópica} se existe $g \in \mor_\Top(Y,X)$ tal que
	\[
		g \circ f \homotiso \id_X \quad \text{e} \quad f \circ g \homotiso \id_Y
	\]
	Neste caso, dizemos que $X$ e $Y$ têm o \textbf{mesmo tipo de homotopia}.
\end{defi}

Desta definição percebemos que $f$ é uma equivalência homotópica se, e somente se, $[f]$ é uma equivalência (no sentido categórico) em $\hTop$. De fato, nas relações da definição podemos substituir os morfismos por suas classes de equivalência e as relações de homotopia por igualdades.

\begin{defi}
	Sejam $X,Y \in \Top$ e $y_0 \in Y$. A \textbf{aplicação constante} em $y_0$ é a função $c : X \to Y$ com $c(x) = y_0$ para todo $x \in X$. A aplicação contínua $f : X \to Y$ é \textbf{homotópica à constante} se existe uma aplicação constante $c : X \to Y$ tal que $f \homotiso c$.
\end{defi}

\begin{teo}
	Seja $f : S^n \to Y$ uma aplicação contínua. Então as seguintes condições são equivalentes:
	
	\begin{enumerate}[label=(\roman*)]
		\item $f$ é homotópica à constante;
		\item $f$ pode ser estendida à uma aplicação contínua $D^{n + 1} \to Y$;
		\item se $x_0 \in S^n$ e $k : S^n \to Y$ é a aplicação constante em $f(x_0)$, então existe uma homotopia $F : f \homotiso k$ com
		\[
			F(x_0, t) = f(x_0) \quad \text{para todo } t \in I.
		\]
	\end{enumerate}
\end{teo}

\begin{defi}
	Um espaço topológico $X$ é \textbf{contrátil} se $\id_X$ é homotópica à constante.
\end{defi}

\begin{teo}
	Todo espaço convexo $X$ é contrátil.
\end{teo}

\begin{teo}
	Seja $X \in Top$. Definimos uma relação binária em $X$ da seguinte maneira: para todo $a,b \in X$
	\[
		a \eqv b \iff \text{ existe um caminho de $a$ em $b$}.
	\].
	Então $\eqv$ é uma relação de equivalência $X$.
\end{teo}

\begin{proof}
	Esse resultado é conhecido de topologia geral. Para a propriedade reflexiva, utilizamos o caminho constante. Para a simétrica utilizamos o caminho inverso e para a propriedade transitiva utilizamos a concatenção de caminhos. Todos estes caminhos serão definidos posteriormente.
\end{proof}

\begin{defi}
	Denotamos por $\pi_0(X)$ o conjunto das classes de equivalência determinadas pela relação acima. Tais classes são chamadas \textbf{componentes conexas} de $X$.
\end{defi}

\begin{teo}
	Fazendo $\pi_0(f) : \pi_0(X) \to \pi_0(Y)$ a função que associa à cada componentes conexa $C$ de $X$ a única componente conexa de $Y$ contendo $f(C)$, temos que $\pi_0: \Top \to \Sets$ é um funtor. Além disso, se $f \homotiso g$ então $\pi_0(f) = \pi_0(g)$.
\end{teo}

\begin{defi}
	Sejam $A \subseteq X$ e $f_0, f_1 : X \to Y$ aplicações contínuas tais que $\restr{f_0}{A} = \restr{f_1}{A}$. Se existe uma homotopia $F : X \times \I\to Y$ entre $f_0$ e $f_1$ tal que
	\[
		F(a,t) = f_0(a) = f_1(a) \quad \text{para todo } a \in A \text{ e todo } t \in \I
	\]
	então dizemos que $F$ é uma \textbf{homotopia relativa} à $A$, denotada por
	\[
		f_0 \homotiso f_1 \rel A.
	\]
	
	Recuperamos a definição de homotopia dada anteriormente quando $A = \emptyset$. Neste caso, $F$ é chamada de \textbf{homotopia livre}.
\end{defi}

\begin{teo}
	Homotopia relativa é uma relação de equivalência em cada conjunto $\mor_\Top(X,Y)$.
\end{teo}

\begin{proof}
	Análogo ao caso da homotopia livre.
\end{proof}

\begin{defi}
	Um \textbf{caminho} em $X$ é uma aplicação contínua $f : \I \to X$. Se $f(0) = a$ e $f(1) = b$, então dizemos que $f$ é um caminho \textbf{de $a$ à $b$}. Além disso, dizemos que $a$ e $b$ são os pontos \textbf{inicial} e \textbf{final} de $f$, denotados por $\alpha(f)$ e $\omega(f)$, respectivamente. Um caminho $f$ é \textbf{fechado} em $x_0 \in X$, se $\alpha(f) = x_0 = \omega(f)$. Uma caminho fechado em $x_0$ também é chamado de \textbf{laço} em $x_0$.
\end{defi}

\section{Construção de Homotopias}

Vamos agora exemplificar algumas homotopias básicas comuns e como construí-las para que possamos utilizá-las em situações mais complexas como na seção seguinte.

Consideremos inicialmente a definição \textit{adjunta} de homotopia. Sejam $f,g : \I \to X$ tais que $F : f \homotiso g$. Definindo
\begin{align*}
	f_t : X &\to Y \\
	x &\mapsto F(x,t)
\end{align*}
podemos interpretar a homotopia $F$ como uma família (de um parâmetro) de funções e dizemos que $f_t$ descreve a deformação no tempo $t$.

\begin{defi}
	Sejam $f,g : \I \to X$ caminhos, definimos então a \textbf{homotopia linear} por
	\begin{align*}
		H : X \times \I &\to X \\
		(x,t) &\mapsto (1 - t) f(x) + t g(x)
	\end{align*}
\end{defi}

\section{Grupos de Homotopia e Dualidade}

Começamos agora a construção dos grupos de homotopia de um espaço topológico qualquer. Inicialmente, realizamos uma construção bastante geométrica do \textit{grupo fundamental} de um espaço à partir das noções de homotopia definidas anteriormente. Uma das propriedades mais importantes deste grupo na caracterização de um espaço é o fato de permitir a detecção de certas classes de \textit{buracos}. Sabendo disso, generalizamos a construção para outras dimensões, a fim de aumentar a classe de buracos que somos capazes de detectar e portanto refinar a caracterização de espaços topológicos.

\begin{defi}
	Sejam $f,g : \I \to X$ caminhos tais que $f(1) = g(0)$. Definiremos um novo caminho $f \star g$ por
	\begin{align*}
	f \star g : \I &\to X \\
	t &\mapsto (f \star g)(t) = \begin{cases}
	f(2t) 		& t \in \left[0,\frac{1}{2}\right] \\
	g(2t - 1) 	& t \in \left[\frac{1}{2},1\right].
	\end{cases}
	\end{align*}
	Tal operação entre caminhos é chamada de \textbf{concatenação} de $f$ e $g$.
\end{defi}

É uma simples consequência do lema da colagem que sob as condições da definição acima $f \star g$ é contínua e portanto um caminho.

\begin{teo}
	Sejam $f_0, f_1, g_0, g_1 : \I \to X$ caminhos em $X$ tais que
	\[
	f_0 \homotiso f_1 \rel \partial \I \quad \text{e} \quad g_0 \homotiso g_1 \rel \partial \I.
	\]
	Se $f_0(1) = f_1(1) = g_0(0) = g_1(0)$, então $f_0 \star g_0 \homotiso f_1 \star g_1 \rel \partial \I$.
\end{teo}

\begin{defi}
	Seja $\partial \I = \{0,1\}$ a fronteira de $\I$ em $\R$. A classe de homotopia de um caminho $f : \I \to X$ relativa à $\partial \I$ é chamada de \textbf{classe de caminhos} de $f$ e é denotada por $[f]$.
\end{defi}

Não há ambiguidade na notação das classes de homotopia e classes de caminho, pois como observado anteriormente a homotopia livre é um caso especial de homotopia relativa.

Para quaisquer dois caminhos $f,g : \I \to X$ tais que $f \homotiso g \rel \partial \I$, então sabemos que $f(0) = g(0)$ e $f(1) = g(1)$, logo faz sentido falar nos pontos \textbf{inicial} e \textbf{final} de uma classe de caminhos $[f]$, isto é, estão definidos $\alpha[f]$ e $\omega[f]$.

\begin{defi}
	Se $p \in X$, então o \textbf{caminho constante} em $p$ é uma aplicação constante $i_p : \I \to X$ em $p$. Se $f : \I \to X$ é uma caminho, seu	 \textbf{inverso} é o caminho
	\begin{align*}
	\overline{f} : \I &\to X \\
	t &\mapsto \overline{f}(t) = f(1 - t).
	\end{align*}
\end{defi}

\begin{teo}
	Se $X \in \Top$, então $\pi_1(X)$ é um grupóide, chamado de \textbf{grupóide fundamental de $X$}, cujos objetos são os pontos de $X$, e para cada $p,q \in X$
	\[
		\mor_{\pi_1(X)}(p,q) = \left\{ [f], \alpha[f] = p \text{ e } \omega[f] = q \right\}.
	\]
	Além disso, para cada $p,q,r \in X$, definimos
	\begin{align*}
		\circ : \mor_{\pi_1(X)}(p,q) \times \mor_{\pi_1(X)}(q,r) &\to \mor_{\pi_1(X)}(p,r) \\
		([f],[g]) &\mapsto [f][g] = [f \star g],
	\end{align*}
	onde para cada $p \in X$, $[i_p]$, a classe do caminho constante em $p$, é a identidade (do objeto $p$) na operação definida acima.
\end{teo}

Vemos que a operação de ``concatenação'' $[f][g]$ de classes de caminhos $[f]$ e $[g]$ definida no teorema acima, só está definida quando $\alpha[f] = \omega[g]$, portanto $\pi_1(X)$ não é um grupo, o que motiva a seguinte definição.

\begin{defi}
	Escolhendo um ponto $x_0 \in X$, podemos considerar o espaço com ponto base $(X,x_0) \in \TopB$. O \textbf{grupo fundamental de $X$ em $x_0$} é
	\[
		\pi_1(X,x_0) = \left\{ [f] : [f] \text{ é uma classe de caminho em } X \text{ com } \alpha[f] = x_0 = \omega[g] \right\}
	\]
	com a operação
	\[
		[f][g] = [f \star g]
	\]
\end{defi}

\begin{teo}
	$\pi_1(X,x_0)$ é um grupo para cada $x_0 \in X$.
\end{teo}

\begin{teo}
	$\pi_1: \TopB \to \Grp$ é um funtor covariante. Além disso, se $h, k : (X,x_0) \to (Y,y_0)$ e $h \homotiso k \rel \, \{x_0\}$, então $\pi_1(h) = \pi_1(k)$.
\end{teo}

Denotamos $\pi_1(h)$ por $h_\star$ e a chamamos de aplicação \textbf{induzida} de $h$.

Podemos considerar um caminho fechado $f : (\I, \partial \I) \to (X,x_0)$ pode ser considerada como uma aplicação $(S^1, 1) \to (X,x_0)$, logo $\pi_1(X,x_0)$ pode ser visto como classes de caminho pontuados de $S^1$ em $X$, o que sugere que podemos considerar o \textbf{grupo de homotopia} $\pi_n(X,x_0)$ como o conjunto de classes de homotopia de aplicações de $S^n$ em $X$, com pontos base convenientes. Os seguintes resultados tornarão esta idéia precisa e mostrarão que $\pi_n(X,x_0)$ é de fato um grupo.

Sob este novo ponto de vista, precisaremos considerar espaços que consistem de caminhos de um espaço dado, portanto vamos estudar espaços de funções.

Vamos considerar o conjunto de todas as funções $Y \to X$, denotado por $X^Y$. Sejam $X,Y,Z$ conjuntos quaisquer e $F : Z \times Y \to X$ uma função de duas variáveis. Para cada $z \in Z$ fixado, $F_z : Y \to X$ é uma família de funções de $Y$ em $X$. De forma explícita, essa família é definida por
\begin{align*}
	F^\sharp : Z &\to X^Y \\
	z &\mapsto F^\sharp(z) = F_z
\end{align*}

\begin{defi}
	Se $F : Z \times Y \to X$ é uma função, então $F^\sharp$ é chamada função \textbf{associada} de $F$.
\end{defi}

O processo inverso também é válido: se $G : Z \to X^Y$, definimos
\begin{align*}
	G^\flat : Z \times Y &\to X \\
	(z,y) &\mapsto G^\flat(z,y) = G(z)(y).
\end{align*}
Portanto $F^\sharp$ é uma bijeção cuja inversa é $G^\flat$. Logo, em $\Sets$ temos a equivalência
\[
	X^{Z \times Y} \catiso \left(X^Y\right)^Z
\]

O objetivo agora é obter um equivalência análoga em $\Top$, isto é, quando $X,Y,Z$ são espaços topológicos. Para isso precisamos que $F^\sharp$ e $G^\flat$ sejam contínuas.

\begin{defi}
	Se $X$ e $Y$ são espaços topológicos e $X^Y$ é o conjunto de todas as funções contínuas $Y \to X$, definimos a \textbf{topologia compacto-aberto} em $X^Y	$ como a topologia cuja sub-base consiste dos conjuntos
	\[
		(K; U) = \left\{ f \in X^Y \| f(K) \subset U \right\},
	\]
	onde $K \subseteq Y$ é compacto e $U \subseteq X$ é aberto. Portanto, um aberto em $X^Y$ é uma união arbitrária de interseções finitas de conjuntos $(K;U)$.
\end{defi}

A topologia compacto-aberto é conveniente, pois quando $X$ é um espaço métrico, tal topologia corresponde à topologia da convergência uniforme em subconjuntos compactos, o que abrange vários espaços de interesse.

\begin{defi}
	Se $Y$ e $X$ são conjuntos, então a \textbf{aplicação avaliação} por
	\begin{align*}
		@ : X^Y \times Y &\to X \\
		(f,y) &\mapsto @(f,y) = f(y)
	\end{align*}
\end{defi}

\begin{teo}
	Sejam $X,Y,Z$ espaços topológicos com $Y$ Hausdorff localmente compacto e $X^Y$ com a topologia compacto-aberto.
	
	\begin{enumerate}[label=(\roman*)]
		\item A aplicação avaliação $@ : X^Y \times Y \to X$ é contínua;
		\item A função $F: Z \times Y \to X$ é contínua se, e somente se, sua associada $F^\sharp : Z \to X^Y$ é contínua.
	\end{enumerate}
\end{teo}

\begin{coro}
	Sejam $X,Y,Z$ espaços topológicos com $Y$ Hausdorff localmente compacto. Uma função $g : Z \to X^Y$ é contínua se, e somente se a composta
	
	\adjustbox{scale=\diagramscale, center} {
		\begin{tikzcd}
			Z \times Y \ar[r, "g \times \id_Y"] & X^Y \times Y \ar[r, "@"] & X
		\end{tikzcd}
	}
	é contínua.
\end{coro}

\begin{lema}
	A categoria $\hTopB$ possui um objeto nulo, produtos finitos e coprodutos finitos.
\end{lema}

\begin{obs}
	Seja $(X, x_0)$ um espaço com ponto base. Quando não houver necessidade de explicitar seu ponto base o denotaremos simplesmente por $X$, caso não haja ambiguidade.
\end{obs}

Nas definições à seguir, justaposição de aplicações é simplesmente a composição usual.

\begin{defi}
	Um espaço com ponto base $(X,x_0)$ é um \textbf{H-grupo} se existem aplicações pontuadas contínuas $\mu : X \times X \to X$ e $\eta : X \to X$ e homotopias (pontuadas), tais que
	
	\begin{align*}
		\mu(\id_X \times \mu) &\homotiso \mu(\mu \times \id_X) \quad \text{(associativide)}; \\
		\mu \in_1 \homotiso &\id_X \homotiso \mu \in_2,
	\end{align*}
	onde $\in_1, \in_2 : X \to X \times X$ são definidas por $\in_1(x) = (x,x_0)$ e $\in_2(x) = (x_0,x)$, e
	\[
		\mu(\id_X, \eta) \homotiso c \homotiso \mu(\eta, \id_X),
	\]
	onde $c : X \to X$ é aplicação constante em $x_0$.
\end{defi}

Para enunciar a definição dual desta última consideremos o produto \textbf{wedge} entre $X$ e $Y$ como
\[
	X \vee Y = X \times \{x_0\} \cup \{x_0\} \times X \subset X \times X.
\]
e as aplicações $q_i = \restr{\pr_i}{X \vee Y}$ para $i=1,2$.

\begin{defi}
	Um espaço com ponto base $(X,x_0)$ é um \textbf{H'-grupo} se existem aplicações pontuadas contínuas $m : X \to X \vee X$ e $h : X \to X$ e homotopias (pontuadas), tais que
	
	\begin{align*}
	(\id_X \vee \, m)m &\homotiso (m \vee \id_X)m \quad \text{(co-associativide)}; \\
	q_1 m \homotiso &\id_X \homotiso q_2 m \\
	(\id_X, h) m \homotiso \: &c \homotiso (h, \id_X) m,
	\end{align*}
	onde $c : X \to X$ é aplicação constante em $x_0$.
\end{defi}

\begin{lema}
	Os objetos-grupo em $\TopB$ são exatamente os H-grupos, e os objetos-cogrupo em $\TopB$ são exatamente os H'-grupos.
\end{lema}

\begin{defi}
	Se $(X,x_0)$ é um espaço com ponto base, então seu \textbf{espaço de laços}, denotado por $\Omega(X,x_0)$, é o espaço de funções
	\[
		\Omega(X,x_0) = (X,x_0)^{(\I, \partial \I)},
	\]
	como sub-espaço de $X^\I$ (com a topologia compacto-aberto). O ponto base de $\Omega(X,x_0)$ é $\omega_0$, o caminho constante em $x_0$.
\end{defi}

\begin{teo}
	O espaço de laços define um funtor $\Omega : \hTopB \to \hTopB$.
\end{teo}

\begin{teo}
	Se $(X,x_0)$ é um espaço com ponto base, então $\Omega(X,x_0)$ é um H-grupo.
\end{teo}

\begin{coro}
	Para qualquer espaço com ponto base $X$,
	\[
		[-, \Omega X] : \hTopB \to \Grp
	\]
	é um funtor contravariante. Se $Y$ é um espaço com ponto base e $[f],[g] \in [Y,\Omega X]$ então seu produto é dado por $\mu([f],[g]) = [f \star g]$
\end{coro}

\begin{defi}
	Se $(Z,z_0)$ é um espaço com ponto base, então a \textbf{suspensão} de $Z$, denotada por $\Sigma Z$, é o espaço quociente
	\[
		\Sigma Z = (Z \times \I)/((Z \times \partial \I) \cup (\{z_0\} \times \I)),
	\]
	onde o subconjunto identificado é tomado como ponto base de $\Sigma Z$.
\end{defi}

\begin{teo}
	A suspensão define um funtor $\Sigma : \hTopB \to \hTopB$.
\end{teo}

\begin{teo}
	$(\Sigma,\Omega)$ é um par de funtores adjuntos em $\hTopB$.
\end{teo}

\begin{coro}
	Se $X$ é um espaço com ponto base, então $\Sigma X$ é um objeto-cogrupo em $\hTopB$.
\end{coro}

\begin{comment}
\begin{defi}
	Seja $X$ um espaço de Hausdorff localmente compacto, e denotemos $\infty$ um ponto que não esteja em $X$. A \textbf{compactificação por um ponto} de $X$, denotada por $X^\infty$, é o conjunto $X^\infty = X \cup \{\infty \}$ cuja topologia consiste dos abertos de $X$ e todos os conjuntos da forma $(X-K) \cup \{\infty\}$, onde $K \subseteq X$ é compacto. $\infty$ é tomado como ponto base para $X^\infty$.
\end{defi}

\begin{defi}
	Se $X$ e $Y$ são espaços com ponto base, seu \textbf{produto smash}, denotado por $X \wedge Y$ é o espaço pontuado
	\[
		X \wedge Y = (X \times Y)/(X \vee Y).
	\]
\end{defi}
\end{comment}

Como observado anteriormente, podemos considerar
\[
	\pi_1(X,x_0) = [S^1, X],
\]
onde $(1,0)$ é o ponto base de $S^1$. Em geral, consideremos $s_n = (1,0,\cdots,0) \in \R^{n+1}$ como ponto base de $S^n$, para todo $n \ge 0$.

\begin{defi}
	Para todo espaço com ponto base $(X,x_0)$ e todo $n \ge 0$,
	\[
		\pi_n(X,x_0) = [(S^n, s_n), (X, x_0)],
	\]
	é o \textbf{grupo de homotopia} de $(X,x_0)$.
\end{defi}

\begin{teo}
	Para todo espaço com ponto base $(X,x_0)$ e todo $n \ge 1$,
	\[
		\pi_n : \hTopB \to \Grp
	\]
	é um funtor. Caso $n \ge 2$, $\pi_n$ é um funtor $\hTopB \to \Ab$.
\end{teo}

Os funtores definidos nessa seção, como já foi dito, são úteis em topologia algébrica, principalmente para a caracterização de espaços e também possuem aplicações em análise, particularmente, na teoria de integração, na qual temos que levar em conta os buracos ``presentes'' no domínio da integração.

%\section{Exercícios}